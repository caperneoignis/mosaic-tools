﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PixelMeasuringTool
{
    public partial class PixelTester : Form
    {
        //so we can save the text after the program has ran.
        System.Text.StringBuilder arrayFromInput = new System.Text.StringBuilder();
        //this program computes the area that a char takes up for use in another program.
        float computeLuminosity(char theChar, int width, int height, string fontName)
        {

            Bitmap bitMap = new Bitmap(width, height);
            Graphics graphics = Graphics.FromImage(bitMap);

            graphics.PageUnit = GraphicsUnit.Point;
            //make a point and then transform said points. 
            Point[] transformPoints = new Point[1];
            transformPoints[0] = new Point(1, 1);

            graphics.TransformPoints(System.Drawing.Drawing2D.CoordinateSpace.Device,
                System.Drawing.Drawing2D.CoordinateSpace.Page, transformPoints);



            Font font = new Font(fontName, 32);

            string theString = new string(theChar, 1);

            graphics.MeasureString(theString, font);
            SizeF pixelsInFont = graphics.MeasureString(theString, font);
            //*****the area that the character is taking up*******
            //This is the business portion. Write a file saving portion.
            write_ToBox(theString, (pixelsInFont.Width/pixelsInFont.Height));
            // graphics.DrawString(theString, font, Brushes.Red, new PointF(0, 0));
            graphics.DrawString(theString, font, Brushes.Black, new RectangleF(0, 0, width, height));

            // graphics.ScaleTransform(width, height);
            //we need to add up all the white pixels and then subtract the black pixels used in the block. 
            float luminosityCount = 0;
            for (int i = 0; i < pixelsInFont.Height; ++i)
            {
                for (int j = 0; j < pixelsInFont.Width; j++)
                {
                    Color pixelColor = bitMap.GetPixel(j, i);
                    luminosityCount += pixelColor.R + pixelColor.G + pixelColor.B;
                }
            }

            const int N_COLORS = 3;
            const int MAX_COLOR_INTENSITY = 255;
            float maxLuminosity = width * height * N_COLORS * MAX_COLOR_INTENSITY;
            //write_ToBox(theString, (luminosityCount / maxLuminosity));
            return luminosityCount / maxLuminosity;
        }

        public PixelTester()
        {
            InitializeComponent();
            //the start button and text box handlers.

            //start the function
            startButton.MouseClick += new MouseEventHandler(startButton_Click);

            //print location
            pictureBox1.Paint += new PaintEventHandler(pictureBox1_Paint);
            //save when clicked
            Save_Button.MouseClick += new MouseEventHandler(Save_Button_click);
        }
        //save to a file after input
        private void Save_Button_click(object sender, EventArgs e)
        {
            //for (int i = 0; i < arrayFromInput.Length; i++)
            //{
            //    arrayFromInput.;
            //}
            System.IO.File.WriteAllText(@"textOutput.txt", arrayFromInput.ToString());
        }
        //invalidate
        private void startButton_Click(object sender, EventArgs e)
        {
            pictureBox1.Invalidate();

        }
        //this program writes to a box
        private void write_ToBox(string letter, float information)
        {

            output.AppendText(letter);
            output.AppendText("   =   ");
            output.AppendText(information.ToString());
            output.AppendText("\n");
            // this appends it to a special text holder, will look like a 0.1232828 etc....
            arrayFromInput.Append(letter);
            arrayFromInput.Append(" ");
            arrayFromInput.Append(information.ToString() + Environment.NewLine);
            //arrayFromInput.Append("\n");
        }
    //the kick off to compute area
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //base.OnPaint(e);
            //Graphics graphics = CreateGraphics();
            //using (graphics = e.Graphics)
            //{
            //    string test = "a";
            //    Font drawFont = new Font("Arial", 16);
            //    SolidBrush drawBrush = new SolidBrush(Color.Black);
            //    PointF drawPoint = new PointF(0,0);
            //    graphics.DrawString(test, drawFont, drawBrush, drawPoint);
            //}

            int width = 45;
            int height = 100;
            string fontName = "Times New Roman";
            char theChar = 'a';
            Bitmap bitMap = new Bitmap(width, height);
            Graphics graphics = Graphics.FromImage(bitMap);

            Font font = new Font(fontName, 32);

            string theString = new string(theChar, 1);

            // graphics.DrawString(theString, font, Brushes.Black, new PointF(0, 0));
            graphics.DrawString(theString, font, Brushes.Black, new RectangleF(0, 0, width, height));

            Graphics formGraphics = e.Graphics;

            // formGraphics.ScaleTransform(2.0f, 2.0f);
            formGraphics.DrawImage(bitMap, new Point(0, 0));

            // graphics.ScaleTransform(width, height);

            float luminosityCount = 0;
            for (int i = 0; i < bitMap.Height; i++)
            {
                for (int j = 0; j < bitMap.Width; j++)
                {
                    Color pixelColor = bitMap.GetPixel(j, i);
                    luminosityCount += pixelColor.R + pixelColor.G + pixelColor.B;
                }
            }

            const int N_COLORS = 3;
            const int MAX_COLOR_INTENSITY = 255;
            float maxLuminosity = bitMap.Width * bitMap.Height * N_COLORS * MAX_COLOR_INTENSITY;

            float percentLuminosity = luminosityCount / maxLuminosity;
            output.Text = "char     luminocity \n";
            for (char c = 'a'; c <= 'z'; c++)
            {
                computeLuminosity(c, bitMap.Width, bitMap.Height, fontName);
            }

            // write_ToBox(theString, percentLuminosity);
            // return luminosityCount / maxLuminosity;


        }

    }
}