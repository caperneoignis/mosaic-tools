﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Windows.Media;

namespace greyImage
{
   
    public partial class Form1 : Form
    {
        public float bigPixelHeight = 1;
        public float bigPixelWidth = 1;
        public string openFileName = "Chrysanthemum.jpg";
        public string saveFileName = "test.bmp";
        public Bitmap fileToBeConverted;

        public Form1()
        {
            InitializeComponent();
            Pixelate(openFileName, saveFileName, bigPixelWidth, bigPixelHeight);
        }

        private void Pixelate(string openFileName, string saveFileName, float bigPixelWidth, 
            float bigPixelHeight)
        {

            Bitmap fileToBeConverted = new Bitmap(openFileName);
            int ARRAY_SIZE = 16;
            Color[] myGreyScale = new Color [ARRAY_SIZE +1];
            int timesThroughLoopForAvg = 0;
            myGreyScale[0] = Color.FromArgb(0);
            for (int i = ARRAY_SIZE; i > 0 ; i--)
            {
                myGreyScale[i] = Color.FromArgb(255 / i);
            }

            const int N_COLORS = 3;
            const int MAX_COLOR_INTENSITY = 255;
            float maxLuminosity = bigPixelWidth * bigPixelHeight * N_COLORS * MAX_COLOR_INTENSITY;

            float maxBigPixelLuminosity = 0;
            float minBigPixelLuminosity = maxLuminosity;
            float totalBigPixelLuminosity = 0;
            float[,] bigPix = new float[(int)(fileToBeConverted.Width / bigPixelWidth), (int)(fileToBeConverted.Height/bigPixelHeight)];
            
            //bigPix = new float(); //initial
            float luminosityCount = 0;
            
            
            //for every big pixel there is an x and width portion

            Color[,] pixelColor = new Color[fileToBeConverted.Width, fileToBeConverted.Height];
            for (int height = 0; height < fileToBeConverted.Height; height++)
            {
                for (int width = 0; width < fileToBeConverted.Width; width++)
                    pixelColor[width, height] = fileToBeConverted.GetPixel(width, height);
            }


            int widthIncrement;
            widthIncrement = 0;
            int heightIncrement;
            heightIncrement = 0;
            int pixelHeight = 0;
            int iBigPixelHeight = (int)bigPixelHeight;
            int iBigPixelWidth = (int)bigPixelWidth;

            for (int height = 0; height < fileToBeConverted.Height-iBigPixelHeight; height = height + iBigPixelHeight)
            {
                for (int width = 0; width < fileToBeConverted.Width-iBigPixelWidth; width = width + iBigPixelWidth)
                {
                    for (pixelHeight = 0; pixelHeight < bigPixelHeight; pixelHeight+=4)
                    {
                        for (int pixelWidth = 0; pixelWidth < bigPixelWidth; pixelWidth+=4)
                        {
                            //add them all togather
                            luminosityCount += pixelColor[width+pixelWidth, height+pixelHeight].R + 
                                               pixelColor[width+pixelWidth, height+pixelHeight].G + 
                                               pixelColor[width+pixelWidth, height+pixelHeight].B;
                            timesThroughLoopForAvg++;
                        }
                    }
                    //divide to get the avg
                    luminosityCount = luminosityCount / maxLuminosity;
                   // timesThroughLoopForAvg = 0; //reset
                    // bigPix[j, x] = new List<float>();
                    //need to change this so not so much crap in it. 
                    bigPix[widthIncrement, heightIncrement] = luminosityCount;
                    widthIncrement++;
                    maxBigPixelLuminosity = (luminosityCount < maxBigPixelLuminosity) ? 
                        maxBigPixelLuminosity : luminosityCount;
                    minBigPixelLuminosity = (luminosityCount > minBigPixelLuminosity) ? 
                        minBigPixelLuminosity : luminosityCount;
                    totalBigPixelLuminosity += luminosityCount;
                    luminosityCount = 0;
                }
                heightIncrement++;
                widthIncrement = 0;
            }


            float averageBigPixelLuminosity =
                totalBigPixelLuminosity / (bigPixelHeight * bigPixelWidth);


            const int SHADES_OF_GREY = 16;
            float dynamicRange = maxBigPixelLuminosity - minBigPixelLuminosity;
            float greyScalar = dynamicRange / SHADES_OF_GREY;
            widthIncrement = 0;
            heightIncrement = 0;
            Graphics graphics = Graphics.FromImage(fileToBeConverted);
           // float lumCount = 0;
            SolidBrush[] greyBrush = new SolidBrush[SHADES_OF_GREY + 1];
            for (int i = 1; i <= SHADES_OF_GREY; i++)
            {
                greyBrush[i-1] = new SolidBrush(Color.FromArgb(16*i-1, 16*i-1, 16*i-1));
            }
            greyBrush[SHADES_OF_GREY] = new SolidBrush(Color.FromArgb(MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY, MAX_COLOR_INTENSITY)); //set the end

            for (int height = 0; height < fileToBeConverted.Height - iBigPixelHeight; height += iBigPixelHeight)
            {
                for (int width = 0; width < fileToBeConverted.Width -iBigPixelWidth; width += iBigPixelWidth)
                {

                    int greyScale = (int)((bigPix[widthIncrement, heightIncrement] - minBigPixelLuminosity) / greyScalar);
                    widthIncrement++;
                    if (greyScale < 0)
                    {
                        greyScale = 0;
                    }
                    SolidBrush myGreyScaleBrush = greyBrush[greyScale];
                    //myGreyScaleBrush.Color
                    
                    //fileToBeConverted.
                   graphics.FillRectangle(myGreyScaleBrush, width ,height, bigPixelWidth, bigPixelHeight);
                   
                }
                heightIncrement++;
                widthIncrement = 0;
            }

            // 
            pictureBox1.Image = fileToBeConverted;
          

            // this is where we are going to go through and 
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void heightBar_Scroll(object sender, EventArgs e)
        {
            
            heightBar.Minimum = 1;
           
            heightBar.Maximum = 20;
            bigPixelHeight = heightBar.Value;
            // 
            pictureBox1.Invalidate();
        }

        private void widthBar_Scroll(object sender, EventArgs e)
        {
            widthBar.Minimum = 1;
            widthBar.Maximum = 20;
            bigPixelWidth = widthBar.Value;
            // Pixelate(openFileName, saveFileName, bigPixelWidth, bigPixelHeight);
            pictureBox1.Invalidate();
        }

        private void execute_Click(object sender, EventArgs e)
        {
            Pixelate(openFileName, saveFileName, bigPixelWidth, bigPixelHeight);
        }

        private void save_Click(object sender, EventArgs e)
        {
            pictureBox1.Image.Save(saveFileName);
        }
    }
}
