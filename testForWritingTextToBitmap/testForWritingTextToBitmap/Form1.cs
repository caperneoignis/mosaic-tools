﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace testForWritingTextToBitmap
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            const int BIG_PIXEL_WIDTH = 10;
            const int BIG_PIXEL_HEIGHT = 10;
            this.SetClientSizeCore(BIG_PIXEL_WIDTH, BIG_PIXEL_HEIGHT);

            for (char letter = '!'; letter <= '~'; ++letter)
            {
                writeImageToFile(BIG_PIXEL_WIDTH, BIG_PIXEL_HEIGHT, letter);
            }
           // letter = 'B';
           // writeImageToFile(BIG_PIXEL_WIDTH, BIG_PIXEL_HEIGHT, letter);
        }

        private void writeImageToFile(int width, int height, char letter)
        {
            // string Hello = "A";
            //StringBuilder stringBuilder = new StringBuilder();
            // stringBuilder.Append(letter);

            // string imageFilePath = "test.bmp";
            // Bitmap newImage = new Bitmap(imageFilePath);
            const float margin = 1.15f;
            Bitmap newImage = new Bitmap((int) (width * margin), (int) (height * margin));

            PointF firstLocation = new PointF(newImage.Width/2,newImage.Height/2);
            Size newSize = new Size(1, 1);
            pictureBox1.Image = newImage;
            
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
           
            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                using (Font arialFont = new Font("Times New Roman", height))
                {
                    graphics.Clear(Color.White);
                    graphics.DrawString(letter.ToString(), arialFont, Brushes.Black, firstLocation, format);
                    pictureBox1.Image.Save("map" + (int)letter + ".jpg", ImageFormat.Jpeg);
                    // graphics.DrawString(Hello2, arialFont, Brushes.Black, secondLocation);
                }
            }
        }
    }
}
