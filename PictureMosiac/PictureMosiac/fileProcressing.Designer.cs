﻿namespace com.leeKirkland
{
    partial class fileProcressing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFiles = new System.Windows.Forms.Button();
            this.saveList = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openMosaicButton = new System.Windows.Forms.Button();
            this.imagePreview = new System.Windows.Forms.PictureBox();
            this.imagePreivewHelper = new System.Windows.Forms.ImageList(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.NumFilesSelected = new System.Windows.Forms.Label();
            this.close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imagePreview)).BeginInit();
            this.SuspendLayout();
            // 
            // openFiles
            // 
            this.openFiles.Location = new System.Drawing.Point(23, 24);
            this.openFiles.Name = "openFiles";
            this.openFiles.Size = new System.Drawing.Size(75, 23);
            this.openFiles.TabIndex = 0;
            this.openFiles.Text = "Open Files";
            this.openFiles.UseVisualStyleBackColor = true;
            this.openFiles.Click += new System.EventHandler(this.openFiles_Click);
            // 
            // saveList
            // 
            this.saveList.Location = new System.Drawing.Point(104, 24);
            this.saveList.Name = "saveList";
            this.saveList.Size = new System.Drawing.Size(75, 23);
            this.saveList.TabIndex = 1;
            this.saveList.Text = "Save List";
            this.saveList.UseVisualStyleBackColor = true;
            this.saveList.Click += new System.EventHandler(this.saveList_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // openMosaicButton
            // 
            this.openMosaicButton.Location = new System.Drawing.Point(23, 591);
            this.openMosaicButton.Name = "openMosaicButton";
            this.openMosaicButton.Size = new System.Drawing.Size(156, 23);
            this.openMosaicButton.TabIndex = 2;
            this.openMosaicButton.Text = "Open Mosaic File List";
            this.openMosaicButton.UseVisualStyleBackColor = true;
            this.openMosaicButton.Click += new System.EventHandler(this.openMosaicButton_click);
            // 
            // imagePreview
            // 
            this.imagePreview.Location = new System.Drawing.Point(12, 53);
            this.imagePreview.Name = "imagePreview";
            this.imagePreview.Size = new System.Drawing.Size(786, 532);
            this.imagePreview.TabIndex = 3;
            this.imagePreview.TabStop = false;
            // 
            // imagePreivewHelper
            // 
            this.imagePreivewHelper.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imagePreivewHelper.ImageSize = new System.Drawing.Size(16, 16);
            this.imagePreivewHelper.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(185, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Number Of Files Selected: ";
            // 
            // NumFilesSelected
            // 
            this.NumFilesSelected.AutoSize = true;
            this.NumFilesSelected.Location = new System.Drawing.Point(324, 29);
            this.NumFilesSelected.Name = "NumFilesSelected";
            this.NumFilesSelected.Size = new System.Drawing.Size(14, 13);
            this.NumFilesSelected.TabIndex = 5;
            this.NumFilesSelected.Text = "#";
            // 
            // close
            // 
            this.close.Location = new System.Drawing.Point(723, 590);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(75, 23);
            this.close.TabIndex = 6;
            this.close.Text = "close";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // fileProcressing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 626);
            this.Controls.Add(this.close);
            this.Controls.Add(this.NumFilesSelected);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imagePreview);
            this.Controls.Add(this.openMosaicButton);
            this.Controls.Add(this.saveList);
            this.Controls.Add(this.openFiles);
            this.Name = "fileProcressing";
            this.Text = "fileProcressing";
            ((System.ComponentModel.ISupportInitialize)(this.imagePreview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openFiles;
        private System.Windows.Forms.Button saveList;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button openMosaicButton;
        private System.Windows.Forms.PictureBox imagePreview;
        private System.Windows.Forms.ImageList imagePreivewHelper;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label NumFilesSelected;
        private System.Windows.Forms.Button close;
    }
}