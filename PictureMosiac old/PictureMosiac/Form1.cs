﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.Windows.Controls;
using System.Drawing.Imaging;
using System.IO;


namespace com.leeKirkland
{
    public partial class MainScreen : Form
    {
        public string nameOfFileToBeUsed;
        static public Image imageToBeUsed;
        //static public Image resetImage;
        static public float bigPixelHeight = 4;
        static public float bigPixelWidth = 4;
        public const int N_COMPONENTS_IN_RGB = 3;
        public const int MAX_COLOR_INTENSITY = 255;
        static public int LUM_TABLE_SIZE = 1000;
        static public LuminositySub letterSubstitute_;
        static private pictureProcess pictureItems_;
        public fileProcressing fileListToBeOpened = new fileProcressing();
        public MainScreen()
        {
            InitializeComponent();
            pictureBox1.MouseClick += new MouseEventHandler(pictureBox1_MouseClick);
            
           // IBigWidth.Text = bigPixelWidth.ToString();
            //IBigHeight.Text = bigPixelHeight.ToString();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (this.pictureBox1.SizeMode == PictureBoxSizeMode.Zoom)
            {
                this.pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            }
            else if (this.pictureBox1.SizeMode == PictureBoxSizeMode.AutoSize)
            {
                this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }


        private void openFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.bmp) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png *.bmp";
                //dlg.Filter = "jpg files (*.jpg) | *.jpg";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    // PictureBox PictureBox1 = new PictureBox()
                    // Create a new Bitmap object from the picture file on disk,
                    // and assign that to the PictureBox.Image property
                    pictureBox1.Image = Image.FromFile(dlg.FileName);
                    imageToBeUsed = Image.FromFile(dlg.FileName);
                    //the back up
                    //resetImage = imageToBeUsed;
                    this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                    // Add the new control to its parent's controls collection
                    this.Controls.Add(pictureBox1);
                }
            }


        }
        private void saveFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            //sets the filter.
            saveFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            //save and image file by setting the title and filter.
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image in the appropriate ImageFormat based upon the
                // File type selected in the dialog box.
                // NOTE that the FilterIndex property is one-based.
                //An example was given in MSDN I used as a base for this section
                //**********************************************************************
                //these are the cases depending on what type the file will be save as
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        pictureBox1.Image.Save(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;

                    case 2:
                        pictureBox1.Image.Save(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
                        break;

                    case 3:
                        pictureBox1.Image.Save(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                }

            }
        }

        private void begin_Click(object sender, EventArgs e)
        {
            if (SelectionBox.SelectedItems.Count == 0)
            {
                MessageBox.Show("please check a box, either ASCII or Pictures");
            }
            //invalidate the old box and write a new item
            if (SelectionBox.GetItemChecked(0) == true)
            {
                ASCIIGenerateValues();
                pictureBox1.Image = MakeGrayscaleAndPixel(imageToBeUsed);
                //pictureBox1.Invalidate();
            }
            else if (SelectionBox.GetItemChecked(1) == true)
            {
                if (fileListToBeOpened.returnInfoOnPictures() ==  null)
                {
                    MessageBox.Show("You need to select images to be used in the mosaic");
                }
                //will put the picture portion here need to make a directory handler and something else
                else
                {
                    trackBar1.Minimum = 20;
                    trackBar1.Maximum = 90;
                    trackBar1.Value = 50;
                    pictureItems_ = fileListToBeOpened.returnInfoOnPictures();
                    readInFilesForMosaic();
                    pictureBox1.Image = MakeGrayscaleAndPixel(imageToBeUsed);
                    pictureBox1.Invalidate();
                }
            }
            
            //set the image to be used to the new image.
            //imageToBeUsed = pictureBox1.Image;


        }

        public void readInFilesForMosaic()
        {
            float luminocity = 0.0f;
            string fileName = string.Empty;
            StringBuilder value = new StringBuilder();
            bool hasNotHitSpace = false;
            StringBuilder file = new StringBuilder();
            bigPixelWidth = trackBar1.Value;
            bigPixelHeight = trackBar1.Value;
            Size newSize = new Size((int)bigPixelHeight, (int)bigPixelWidth);
            letterSubstitute_ = new LuminositySub(LUM_TABLE_SIZE, new Bitmap(pictureItems_.returnFileNamLoc(0)));
            for(int i = 0; i < pictureItems_.size(); i++)
            {
                letterSubstitute_.addLetter(pictureItems_.returnPictureLum(i), new Bitmap(pictureItems_.returnFileNamLoc(i)));
            }
               

        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Font font = new Font("Times New Roman", 72);
            Graphics graphics = CreateGraphics();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            trackBar1.Minimum = 4;
            trackBar1.Maximum = 20;
            sizeSelected.Text = trackBar1.Value.ToString();
            bigPixelHeight = trackBar1.Value;
            bigPixelWidth = trackBar1.Value;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        //the ASCII generator
        private void ASCIIGenerateValues()
        {
           
            float luminosityCount = 0;
            
            Size newSize = new Size((int)bigPixelHeight, (int)bigPixelWidth);
            //graphics
            Graphics g;
            //g.FillRegion(Color.White,Region.);
            IBigWidth.Text = newSize.Width.ToString();
            IBigHeight.Text = newSize.Height.ToString();
            //fileToBeMeasured.
            letterSubstitute_ = new LuminositySub(LUM_TABLE_SIZE, new Bitmap(new Bitmap("charPictures/map" + (int)'!' + ".jpg"),newSize));
            float maxLuminosity = bigPixelHeight * bigPixelWidth * N_COMPONENTS_IN_RGB * MAX_COLOR_INTENSITY;
            //for each line in text go through and break it down
            //int width = 0;
            for (char letter = '!'; letter <= '~'; ++letter)
            {
               Bitmap fileToBeMeasured = new Bitmap("charPictures/map" + (int)letter + ".jpg");
                //resize image
               
                for (int pixelHeight = 0; pixelHeight < fileToBeMeasured.Height; pixelHeight += (int) bigPixelHeight)
                {
                    for (int pixelWidth = 0; pixelWidth < fileToBeMeasured.Width; pixelWidth += (int)bigPixelHeight)
                    {
                        //add them all together
                        luminosityCount += fileToBeMeasured.GetPixel(pixelWidth, pixelHeight).R +
                                           fileToBeMeasured.GetPixel(pixelWidth, pixelHeight).G +
                                           fileToBeMeasured.GetPixel(pixelWidth, pixelHeight).B;
                    }
                    //letterArrayLookUp.Add(luminosityCount, fromFloat);
                }
                
                luminosityCount = luminosityCount / maxLuminosity;
               
                //**************************************************
                // the new lumSub that was created to add stuff
                fileToBeMeasured = new Bitmap(fileToBeMeasured, newSize); //resize it before we put it in the add leter.
                letterSubstitute_.addLetter(luminosityCount, fileToBeMeasured);
                //fileToBeMeasured.Dispose(); //ensure it is being getton ride of.
            }

        }
     
        public static int RoundOff(int i)
        {
            return ((int)Math.Round(i / 10.0)) * 10;
        }

        public Image MakeGrayscaleAndPixel(Image original)
        {
            //create a blank bitmap the same size as original
            Bitmap fileToBeConverted = new Bitmap(original);
            Graphics graphics = Graphics.FromImage(fileToBeConverted);
            
            //float maxLuminosity = fileToBeConverted.Width * fileToBeConverted.Height * N_COMPONENTS_IN_RGB * MAX_COLOR_INTENSITY;
            float maxLuminosity = bigPixelWidth * bigPixelHeight * N_COMPONENTS_IN_RGB * MAX_COLOR_INTENSITY;
           // float maxBigPixelLuminosity = 0;
            float minBigPixelLuminosity = maxLuminosity;
           // float totalBigPixelLuminosity = 0;
            //float[,] bigPix = new float[(int)(fileToBeConverted.Width / bigPixelWidth), (int)(fileToBeConverted.Height / bigPixelHeight)];

            //bigPix = new float(); //initial
            float luminosityCount = 0;


            //int widthIncrement;
           // widthIncrement = 0;
            int heightIncrement;
            heightIncrement = 0;
            int pixelHeight = 0;
            int iBigPixelHeight = (int)bigPixelHeight;
            int iBigPixelWidth = (int)bigPixelWidth;
            Size mySize = new Size(iBigPixelWidth, iBigPixelHeight);
            
            //TextureBrush myTexture =  new TextureBrush(myLumSub.getBestLuminosityMatch(0));
            for (int height = 0; height < fileToBeConverted.Height - iBigPixelHeight; height = height + iBigPixelHeight)
            {
                for (int width = 0; width < fileToBeConverted.Width - iBigPixelWidth; width = width + iBigPixelWidth)
                {
                    for (pixelHeight = 0; pixelHeight < bigPixelHeight ; pixelHeight +=4)
                    {
                        for (int pixelWidth = 0; pixelWidth < bigPixelWidth; pixelWidth += 4)
                        {
                            //add them all together
                            luminosityCount +=  fileToBeConverted.GetPixel(width + pixelWidth, height + pixelHeight).R +
                                                fileToBeConverted.GetPixel(width + pixelWidth, height + pixelHeight).G +
                                                fileToBeConverted.GetPixel(width + pixelWidth, height + pixelHeight).B;
                        }
                    }
                    TextureBrush myBrush;
                    //divide to get the avg
                    luminosityCount = luminosityCount / maxLuminosity;
                    // timesThroughLoopForAvg = 0; //reset
                    // bigPix[j, x] = new List<float>();
                    //need to change this so not so much crap in it. 
                    //bigPix[widthIncrement, heightIncrement] = luminosityCount;
                   // widthIncrement++;
                   // maxBigPixelLuminosity = (luminosityCount < maxBigPixelLuminosity) ?
                    //    maxBigPixelLuminosity : luminosityCount;
                   // minBigPixelLuminosity = (luminosityCount > minBigPixelLuminosity) ?
                    //    minBigPixelLuminosity : luminosityCount;
                    //totalBigPixelLuminosity += luminosityCount;
                    myBrush = letterSubstitute_.getBestLuminosityMatch(luminosityCount);
                    //myTexture = TextureBrush(myLumSub.getBestLuminosityMatch(luminosityCount));
                    graphics.FillRectangle(myBrush, new Rectangle(new Point(width, height), mySize));
                    luminosityCount = 0;
                    //graphics.Dispose();
                }
                heightIncrement++;
                //widthIncrement = 0;
            }

           // float averageBigPixelLuminosity =
            //    totalBigPixelLuminosity / (bigPixelHeight * bigPixelWidth);

            //this is the section that will be changing to do this with chars instead of grey......
            //should be fun!
           // const int SHADES_OF_GREY = 16;
            //const int CHARS_FOR_FILL = 26;
           // float dynamicRange = maxBigPixelLuminosity - minBigPixelLuminosity;
           // float greyScalar = dynamicRange / SHADES_OF_GREY;
            //widthIncrement = 0;
            //heightIncrement = 0;
           
            return fileToBeConverted;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void ResetBttn_Click(object sender, EventArgs e)
        {
            //imageToBeUsed = resetImage;
            pictureBox1.Invalidate();
            pictureBox1.Image = imageToBeUsed;
        }

        //we check to see if the only one of the items is checked if so, uncheck the other item and let them reselect the item
        private void SelectionBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (SelectionBox.CheckedItems.Count > 1)
            {
                for (int i = 0; i < SelectionBox.Items.Count; i++)
                {
                    if (SelectionBox.GetItemChecked(i))
                    {
                        SelectionBox.SetItemChecked(i, false);
                    }
                }
            }
        }

        private void OpenDirectory_Click(object sender, EventArgs e)
        {
               /*this.openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.bmp) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png *.bmp";
               this.openFileDialog1.Multiselect = true;
               this.openFileDialog1.Title = "Select picture for Mosiac";
                if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    // PictureBox PictureBox1 = new PictureBox()
                    // Create a new Bitmap object from the picture file on disk,
                    // and assign that to the PictureBox.Image property
                    //pictureBox1.Image = Image.FromFile(dlg.FileName);
                    filePaths = this.openFileDialog1.FileNames;
                }*/
            
            fileListToBeOpened.Show();
        }
        
    }
}



