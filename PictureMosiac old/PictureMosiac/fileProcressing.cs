﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace com.leeKirkland
{
    public partial class fileProcressing : Form
    {
        private List<string> fileNames = new List<string>();
        private List<float> values = new List<float>();
        private pictureProcess pictureItems_;
        
        public fileProcressing()
        {
            InitializeComponent();
        }

        private void openFiles_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.bmp) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png *.bmp";
            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.Title = "Select picture for Mosiac";
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // PictureBox PictureBox1 = new PictureBox()
                // Create a new Bitmap object from the picture file on disk,
                // and assign that to the PictureBox.Image property
                //pictureBox1.Image = Image.FromFile(dlg.FileName);
                filePaths = this.openFileDialog1.FileNames;
                pictureItems_ = new pictureProcess(filePaths.Length);
                NumFilesSelected.Text = filePaths.Length.ToString();
                //previewHelper();
                readInFilesForMosaic();
            }
           
        }
        public pictureProcess returnInfoOnPictures()
        {
            return pictureItems_;
        }

        private void previewHelper()
        {
            imagePreview.Image = new Bitmap(imagePreview.Width, imagePreview.Height);
            Graphics graphics = Graphics.FromImage(imagePreview.Image);
            int counter = 0;
            float incrementerWidth;
            float incrementerHeight;
            int sizeOflimit = pictureItems_.size();
            Size mySize = new Size();
            //if (sizeOflimit > 50)
            //{
            //    incrementerWidth =  50;
            //    incrementerHeight =  50;
            //    sizeOflimit = 50;
            //}
            //else
            //{
                incrementerWidth =  50;
                incrementerHeight = 50;
            //}
            
            mySize.Height = (int)incrementerHeight;
            mySize.Width = (int)incrementerWidth;
            int x = 0;
            int y = 0;
            TextureBrush myBrush;
            while ( x < imagePreview.Height && counter < sizeOflimit)
            {
                while (y < imagePreview.Width && counter < sizeOflimit)
                {
                    myBrush = new TextureBrush(new Bitmap(new Bitmap(pictureItems_.returnFileNamLoc(counter)), mySize));
                    graphics.FillRectangle(myBrush, new Rectangle(new Point(x, y), mySize));
                    ++counter;
                    //counter = counter % pictureItems_.size();
                    y += (int)incrementerHeight;
                }
                y = 0;
                x += (int)incrementerWidth;
            }
            
            
        }
        private void saveList_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            //sets the filter.
            //saveFileDialog1.Filter = "Mosiac File|*.MosFile";
            saveFileDialog1.Filter = "mosiac file| *.MSF";
            //save and image file by setting the title and filter.
            saveFileDialog1.Title = "Save the picture Info File";
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Stream stuffToSave = File.Create(saveFileDialog1.FileName);
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(stuffToSave, pictureItems_);
                stuffToSave.Close();
            }
            //// redo this
            //if (saveFileDialog1.FileName != "")
            //{
            //    //use for later
            //    mosaicFileName = saveFileDialog1.FileName;
            //    System.IO.StreamWriter file = new System.IO.StreamWriter(saveFileDialog1.FileName);
            //    //write the lines to the document.
            //    for (int i = 0; i < fileNames.Count; i++ )
            //    {
            //        file.WriteLine(fileNames[i]);
            //        file.WriteLine(values[i]);
            //    }
                    
            //}

        }
        //public List<string> mosaicFile ()
        //{
        //    return fileNames;
        //}
        //need to add a function that also determines if the image is centered or not and we will use that for rotation. 
        public void readInFilesForMosaic()
        {
            //loop through the image getting all the information about it.
            //pictureInfoForProgram = string.Empty;
            //string[] test = filePaths;
            int bigPixel = 4;
            int bigMulti = 10;
            for (int i = 0; i < filePaths.Length; i++)
            {
                //string fileInfo;
    
                Bitmap fileToBeMeasured = new Bitmap(filePaths[i]);
                
                //resize image
                float luminosityCount = 0;
                float totalRunThroughForAvg = 0; //instead of figuring out height width and all that we just take the total run throughs and do this.
                for (int pixelHeight = 0; pixelHeight < fileToBeMeasured.Height; pixelHeight += bigPixel * bigMulti)
                {
                    for (int pixelWidth = 0; pixelWidth < fileToBeMeasured.Width; pixelWidth += bigPixel * bigMulti)
                    {
                        //add them all together
                        luminosityCount += fileToBeMeasured.GetPixel(pixelWidth, pixelHeight).R +
                                           fileToBeMeasured.GetPixel(pixelWidth, pixelHeight).G +
                                           fileToBeMeasured.GetPixel(pixelWidth, pixelHeight).B;
                        totalRunThroughForAvg++; //add up how many times this executes then divide that. 
                    }
                    //letterArrayLookUp.Add(luminosityCount, fromFloat);
                }

                float maxLuminosity = totalRunThroughForAvg * N_COMPONENTS_IN_RGB * MAX_COLOR_INTENSITY;
                luminosityCount = luminosityCount / maxLuminosity;
                //appened luminosity cout and the file path to the end of the string for storing.
                //fileInfo = ;
                //store it in or fancy new place
                pictureItems_.setInfo(luminosityCount, filePaths[i]);
                ///values.Add(luminosityCount);
                //now append to our database thing
                //fileNames.Add(filePaths[i]);
                //pictureInfoForProgram[i] = fileInfo;
                fileToBeMeasured.Dispose();
            }
        }

        private void openMosaicButton_click(object sender, EventArgs e)
        {
            this.openFileDialog1.Filter = "mosiac file| *.MSF";
            this.openFileDialog1.Title = "Select picture for Mosiac";
            //string[] temp;
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
               //this will be the name that is returned to the program.
                mosaicFileName = this.openFileDialog1.FileName;
                Stream file = File.OpenRead(mosaicFileName);
                BinaryFormatter deserializer = new BinaryFormatter();
                pictureItems_ = (pictureProcess)deserializer.Deserialize(file);
                file.Close();
                NumFilesSelected.Text = pictureItems_.size().ToString();
                filePaths = new string[pictureItems_.size()];
                //for (int i = 0; i < pictureItems_.size(); i++)
                //{
                //    filePaths[i] = pictureItems_.returnFileNamLoc(i);
                //}
                //previewHelper();
            }
            
           
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
         
        public const int N_COMPONENTS_IN_RGB = 3;
        public const int MAX_COLOR_INTENSITY = 255;
        static public string[] filePaths;
        static private string mosaicFileName;
    }
}
