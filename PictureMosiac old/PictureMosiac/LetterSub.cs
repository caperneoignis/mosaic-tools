﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace com.leeKirkland
{
    public class LuminositySub
    {

        public LuminositySub(int luminosityTablesize, Image zeroMap)
        {
            luminosityTablesize_ = luminosityTablesize;
            luminosityTable_ = new Tuple<float, TextureBrush>[luminosityTablesize];

            for (int i = 0; i < luminosityTablesize_; i++)
            {
                luminosityTable_[i] = new Tuple<float, TextureBrush>(0.0f, new TextureBrush(zeroMap));
            }
        }

        public void addLetter(float luminosity, Image bitMap)
        {
            float binLuminosity = 0.0f;
            float tableResolution = 1.0f/luminosityTablesize_;

            Tuple<float, TextureBrush> newEntry = new Tuple<float, TextureBrush>(luminosity, new TextureBrush(bitMap));

            for(int i=0;i<luminosityTablesize_;i++)
            {
                // When the added luminosity is close to the bin
                if (Math.Abs(luminosityTable_[i].Item1 - binLuminosity) >
                   Math.Abs(luminosity - binLuminosity))
                {
                    luminosityTable_[i] = newEntry;
                }
                binLuminosity += tableResolution;
            }

        }

        public TextureBrush getBestLuminosityMatch(float luminosity)
        {
             int index = (int) (luminosity * luminosityTablesize_);
             return luminosityTable_[index].Item2;
        }

        private Tuple<float, TextureBrush>[] luminosityTable_;
        private int luminosityTablesize_;
    }
}
