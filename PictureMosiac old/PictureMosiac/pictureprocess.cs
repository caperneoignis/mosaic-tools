﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.leeKirkland
{
    [Serializable]
    public class pictureProcess
    {
        //intial sets
        //private string locationAndNameOfFile_ = string.Empty;
        //private float pictureLumnocity_ = 0.0f;
        private Tuple<float, string>[] PictureInfo_;
        private int size_ = 0;
        public pictureProcess()
        {

        }
        public pictureProcess(int sizeOfTuple)
        {
            PictureInfo_ = new Tuple<float, string>[sizeOfTuple];
        }
        public pictureProcess(pictureProcess rhs)
        {
            this.size_ = rhs.size_;
            this.PictureInfo_ = rhs.PictureInfo_;
            
        }
        public void setInfo(float value, string fileName)
        {
            Tuple<float, string> newEntry = new Tuple<float, string>(value, fileName);
            PictureInfo_[size_] = newEntry;
            size_++;
        }
        public int size()
        {
            return size_;
        }

        public string returnFileNamLoc(int index)
        {
            return PictureInfo_[index].Item2;
        }
        public float returnPictureLum(int index)
        {
            return PictureInfo_[index].Item1;
        }
    }
}
